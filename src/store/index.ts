import { createStore } from "vuex";
import VuexPersist from "vuex-persist";

const vuexLocalStorage = new VuexPersist({
  key: "vuex",
  storage: window.localStorage,
});

export default createStore({
  state: {
    userInformation: [],
  },
  plugins: [vuexLocalStorage.plugin],
  mutations: {
    setUserInformation(state, payload) {
      state.userInformation = payload.userInformation;
    },
  },
  actions: {},
  modules: {},
});